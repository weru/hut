---
layout: default
title: About Us
description: >-
  We are a tech startup that help businesses broaden their markets by
  cultivating a strong online presence.
permalink: /about/
---
<section class = 'sasa goal art'>
    <div class = 'overlay flex-in'>
        <div>
        <h1>We empowers entrepreneurs</h1>
        <p class = 'half'>
            {{ site.description }} 
            Our intent is to be Africa’s leading provider of affordable web-tech solutions for small businesses.
        </p>
        <a  href = '/solutions' class = 'border'>Learn More</a>
        </div>
    </div>
</section>
<section class = 'team transparent'>
    <h1>Meet the team<i class = 'icon icon-elite green'></i></h1>
    <div class = 'flex-in'>
        {% for member in site.data.team %}
        <div class = 'child duo'>
            <img src = '{{site.baseurl}}/assets/{{ member.name }}.jpg'>
            <a class = 'team-f' a href = '{{ member.linkedin }}' target = '_blank'></a>
            <a class = 'team-f' a href = '{{ member.twitter }}' target = '_blank'></a>
            <div class = 'wisdom'>
            <p>{{ member.bio }}</p>
            </div>
        </div>
        {% endfor %}
    </div>
</section>
<section class = 'transparent services'>
    <h1>Our Team is</h1>
    <p><span class = 'special-border'></span></p>
    <div class = 'flex'>
        <div class = 'child trio dark'>
        <div class = 'our-value'><p>Creative</p></div>
        <p>
            We are always at per with prevailing trends. More importantly, we are
            equally mindful of originality. Everyday, we set out to do something creative - something
            new - something bold.
        </p>
        </div>
        <div class = 'child trio dark'>
        <div class = 'our-value second'><p>Innovative</p></div>
        <p class = 'center-text'>
            Our primary focus is provide tangible value. We are ever looking for ways to give our customers
            a better edge. Deep down, we really believe that every product can be made better.
        </p>
        </div>
        <div class = 'child trio dark'>
            <div class = 'our-value'><p>Thorough</p></div>
            <p>
                Proper execution will turn even the humbliest of ideas into an absolute marvel. At Warmhut,
                we arm ourselves with this mindset. We believe that anything worth doing is worth doing well.
            </p>
        </div>
    </div>
</section>
