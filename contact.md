---
layout: simple
title: Contact Warmhut
noindex: 'true'
permalink: /contact-us/
published: true
---
<style>
 {% include css/form.css %}
</style>
<section class = 'form'>
  <div class = 'previous'><i class = 'icon icon-close'></i></div>
  <div class = 'widget flex-in'>
    <form action = 'https://formspree.io/support@warmhutgroup.com' method = 'POST' id = 'enquire'>
      <div class = 'funga'><select name="Subject" id="Subject" class="select subject" placeholder="I would like to" required>
        <option value = 'Make a general inquiry'>Make a general inquiry</option>
        <option value = 'Request a price quote'>Request a price quote</option>
        <option value = 'Subscribe for mail updates'>Subscribe for mail updates</option>
      </select></div>
      <input type = 'text' name = 'name' id = 'name' placeholder = 'Name' required>
      <input type = 'email' name = '_replyto' id = 'email' placeholder = 'Email address' required>
      <input type = 'submit' class = 'submit' value = 'Send'> 
    </form>
  </div>
</section>
