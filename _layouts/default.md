<!DOCTYPE html>
<html lang="{{ page.lang | default: site.lang | default: "en" }}">
  {% include head.md %}

  <body>
    {% include header.md %}
    <main>
        {{ content }}
    </main>
    {% include chat.md %}
    {% include footer.md %}
  </body>
</html>
