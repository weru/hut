---
layout: default
comments: true
published: true
---
<div class = 'cool libro'>
 <div class = 'overlay'>
   <div class="post-header">
    <div class = 'cover'>
      <time datetime="{{ page.date | date_to_xmlschema }}" itemprop="datePublished">
        <h4 class = 'fade'>
        {{ page.date | date: '%b %d' }} <span class = 'separate'>|</span>
        {% assign time = content| number_of_words | divided_by:170 %}{{ time }} min read
        </h4>
      </time>
      <h1 class="post-title" itemprop="name headline">{{ page.title | escape }}</h1>
      <div class = 'flex-in'><img class = 'gravatar' src = '{{site.baseurl}}/assets/{{page.author}}.jpg' alt = 'post author'></div>
      <h4 itemprop="name" class = 'flex-in'><a href = ''>{{ page.author }}</a></h4>
      <h4 itemprop='author description' class = 'fade'>{{ site.status}}</h4>
      {% if page.author %}
        <span itemprop="author" itemscope itemtype="http://schema.org/Person">
          <a class = 'connect' href = 'https://mobile.twitter.com/warmhutgroup' target = '_blank'>Follow</a>
        </span>
      {% endif %}
    </div>
   </div>
 </div>
</div>
<article class = 'flex post blog'>
  <div class = 'child strip'>
    <div class = 'tablet'>
      {% include share.md %}
    </div>
  </div>
  <div class = 'child tripple'>
    <div class="post-content" itemprop="articleBody">
      {{ content }}
      {% include mobile.md %}
    </div>
    {% if site.disqus.shortname %}
      {% include disqus.md %}
    {% endif %}
  </div>
  <aside class = 'child trio'>
    <h3><span class = 'pretty'>Recent Posts</span></h3>
    <ul class="post-list">
      {% for post in site.posts limit:6 %}
      {% if post != page %}
        <li>
          <a class="post-link" href="{{ post.url | relative_url }}"><i class="icon icon-book" aria-hidden = 'true'></i>{{ post.title | escape }}</a>
        </li>
      {% endif %}
      {% endfor %}
    </ul>
    <h3><span class = 'pretty'>Twitter Feed</span></h3>
    <a class="twitter-timeline" href="https://twitter.com/warmhutGroup" data-tweet-limit="3"></a>
    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
  </aside>
</article>
