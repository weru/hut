(function(){
      mailForm = $('#enquire');
    widget = $('.widget');
    mailForm.submit(function(e) {
      guest = document.getElementById('name').value.toUpperCase();
      e.preventDefault();
      if(guest){
        $.ajax({
          url: '//formspree.io/support@warmhutgroup.com',
          method: 'POST',
          data: $(this).serialize(),
          dataType: 'json',
          beforeSend: function() {
            widget.html(`
              <h2>Sending</h2>
              <span class = 'spinner'></span>
            `);
          },
          success: function(data) {
            widget.html(`
              <div class = "confirm">
                <h2>Hey ${guest}!</h2>
                <div class = 'bigicon  success'><i class = 'icon iconcool'></i></div>
                <p>We received your request. We will get back to you asap.</p>
              </div>
            `);
          },
          error: function(err) {
            widget.html(`
              <div class = "confirm">
                <h2 class = 'error'>Yikes! There was problem.</h2>
                <p>Please refrsh and try again.</p>
              </div>
            `);
          }
        });
      }
    });
})();

(function() {
  'use strict';
   var bot, input, message, email, guest, mailForm, year, widget;
   $(function() {
       year = new Date();
       $('.thisYear').append(year.getFullYear());
       $('.view-menu').on('click', function(){
         $('header').toggleClass('open');
         $(this).children().toggleClass('icon-close').toggleClass('icon-bars');
         $(this).toggleClass('view-menu').toggleClass('close-bars');
       });
   });

   $('.previous').on('click', function() {
     window.history.back();
     console.log('I want to go back');
   });

  $(".select").each(function() {
     var classes = $(this).attr("class"),
     id      = $(this).attr("id"),
     name    = $(this).attr("name");
     var template =  '<div class="' + classes + '">';
     template += '<span class="gun">' + $(this).attr("placeholder") + '</span>';
     template += '<div class="options">';
     $(this).find("option").each(function() {
       template += '<span class="option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
     });
     template += '</div></div>';
     
     $(this).hide();
     $(this).after(template);
   });
   $(".option:first-of-type").hover(function() {
     $(this).parents(".options").addClass("option-hover");
   }, function() {
     $(this).parents(".options").removeClass("option-hover");
   });
   $(".gun").on("click", function() {
     $('html').one('click',function() {
       $(".select").removeClass("opened");
     });
     $(this).parents(".select").toggleClass("opened");
     event.stopPropagation();
   });
   $(".option").on("click", function() {
     $(this).parents(".funga").find("select").val($(this).data("value"));
     $(this).parents(".options").find(".option").removeClass("selection");
     $(this).addClass("selection");
     $(this).parents(".select").removeClass("opened");
     $(this).parents(".select").find(".gun").text($(this).text());
   });
})();

(function() {
    var font, s;
    font = document.createElement('link'); 
    font.type = 'text/css'; 
    font.rel = 'stylesheet';
    font.href = 'https://fonts.googleapis.com/css?family=Karla:400,700,700i|Lora:400,400i,700,700i|Nunito+Sans:300,300i,400,400i,600,600i';
    s = document.getElementsByTagName('link')[0];
    s.parentNode.insertBefore(font, s);
    var icons, t;
    icons = document.createElement('link'); 
    icons.type = 'text/css'; 
    icons.rel = 'stylesheet';
    icons.href = 'https://file.myfontastic.com/XJbAgRNrWhky7heFpqzdyn/icons.css';
    t = document.getElementsByTagName('link')[1];
    t.parentNode.insertBefore(icons, t);
  })();

  window.onload = function() {
    var video, cheza, chill, full, place, sauti, videoIcon, close;
    //video icon
    videoIcon = document.getElementById('our-video');
    close = document.getElementById('close');
    //video
    video = document.getElementById('video');
    //buttons
    cheza = document.getElementById('play');
    chill = document.getElementById('mute');
    full = document.getElementById('full');
    //sliders
    place = document.getElementById('progress');
    sauti = document.getElementById('volume');
  
    if(video){
      //popup video
      videoIcon.addEventListener('click', function() {
        $('.media').addClass('video');
      });
      close.addEventListener('click', function() {
        $('.media').removeClass('video');
      });
      //cheza video
      video.volume = 0.1;
      cheza.addEventListener('click', function() {
        if (video.paused) {
          video.play();
          $(this).toggleClass('tall');
          $(this).children().removeClass('icon-play').addClass('icon-pause');
        } else {
          video.pause();
          $(this).children().removeClass('icon-pause').addClass('icon-play');
          $(this).toggleClass('tall');
        }
      });
    
    //chill sauti ya video
      chill.addEventListener('click', function() {
        if (!video.muted) {
          video.muted = true;
          $(this).children().removeClass('icon-mute').addClass('icon-unmute');
        } else {
          video.muted = false;
          $(this).children().removeClass('icon-unmute').addClass('icon-mute');
        }
      });
    
      //Toggle Fullscreen Mode
      full.addEventListener('click', function() {
        if (video.requestFullscreen) {
          video.requestFullscreen();
        }else if (video.mozRequestFullScreen) {
          video.mozRequestFullScreen();
        } else if (video.webkitRequestFullscreen) {
          video.webkitRequestFullscreen(); 
        }
      });
    
      //update seek bar
      place.addEventListener('change', function() {
        //calculate current time
        var time = video.duration * (place.value/100);
        video.currentTime = time;
        
      });
      
      video.addEventListener('timeupdate', function() {
      //update current time on progressbar
        var value = (100/video.duration) * video.currentTime;
        place.value = value;
      });
    
      place.addEventListener('mousedown', function() {
        video.pause();
      });
      place.addEventListener('mouseup', function() {
        video.play();
      })
    
      //Fix the volume bar
      sauti.addEventListener('change', function() {
        video.volume = sauti.value;
      });
    
      //On finish show a replay button
      video.addEventListener('ended', function(){
      $('.play').children().removeClass('icon-pause').addClass('icon-rewind'); 
      $('.play').addClass('tall');
      console.log('video complete');
      });
    }
  }