<footer class="center-text">
    <div class="flex">
      <div class="child trio">
        <h4>Useful Links</h4>
        <ul>
          <!-- <li><a href="/blog/" title = 'Read our blog'>Blog</a></li> -->
           <li><a href = '/contact-us/' title = 'enquire about our pricing'>Pricing</a></li>
        </ul>
      </div>
      <div class="child trio">
        <h4>Warmhut</h4>
        <ul>
          <li><a href="/solutions/" title = ''>Brand Design</a></li>
          <li><a href="/solutions/" title = 'start or upgrade your website'>Start a Website</a></li>
        </ul>
      </div>
      <div class="child trio">
        <h4>Follow Warmhut</h4>
        <div class = 'flex' id = 'footer-social'>
          <a href = '{{site.facebook}}' target = '_blank' rel = 'noopener' title = 'follow' class = 'flex-in facebook'><i class='icon icon-facebook' aria-hidden='true'></i></a>
          <a href = '{{site.linkedin}}' target = '_blank' rel = 'noopener' title = 'follow' class = 'flex-in linkedin'><i class='icon icon-linkedin' aria-hidden='true'></i></a>
          <a href = '{{site.twitter}}' target = '_blank' rel = 'noopener' title = 'follow' class = 'flex-in twitter'><i class='icon icon-twitter' aria-hidden='true'></i></a>   
        </div>
      </div>
    </div>
    <p class = 'center-text'>
      <a href = '/'>Copyrights  &copy; <span class = 'thisYear'></span> WarmHut</a>
    </p>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src='{{site.baseurl}}/site.js'></script>
</footer>
