<style>
  {% include css/header.css %}
</style>
<header>
  <div class = 'nav-brand'>
    <div class = 'view-menu'>
      <span class = 'icon icon-bars'></span>
    </div>
    <a href='{{ '/' | relative_url}}' class = 'brand'>
      <img src = '/assets/warmhut.png' alt = 'warmhut logo' >
    </a>
  </div>
  <nav class = 'menu'>
    {% for link in site.data.links %}
      <a class = 'nav-item' href='{{site.baseurl}}/{{ link }}/'> {{ link }}</a>
    {% endfor %}    
  </nav>
</header>
