<section class = 'weather'>
  <section class = 'about'>
    <div class = 'transparent'>
      <div class = 'half'>
        <h1>A website for your business</h1>
        <p>We are a team of talented Web developers, ready to go the extra mile and build fast, responsive and functional website for your business.
          Probably what distinguishes us from the crowd is our unrivalled affinity for results.
        </p>
      </div>
    </div>
  </section>
  <section>
    <h1>What do you get?</h1>
    <div class = 'flex tree'>
      <div class = 'child duo'>
        <h2>Optimal Web Hosting</h2>
        <p>
          Depending on your specific preferences, we will adapt an optimal hosting solution for you business.
          We are particularly determined to ensure 24-hrs of uptime for your site at the lowest possible cost.
        </p>
      </div>
      <div class = 'child duo order service-icon'>
        <i class = 'icon icon-cloud'></i>
      </div>
    </div>
    <div class = 'flex tree'>
      <div class = 'child duo service-icon'>
         <i class = 'icon icon-laptop'></i>
      </div>
      <div class = 'child duo'>
        <h2>Responsive Design</h2>
        <p>
          Responsive design enables you to have a version of your website that adapts to different screen
          sizes. This approach is equally great for seo as search bots become more keen on responsive design.
        </p>
      </div>
    </div>
    <div class = 'flex tree'>
      <div class = 'child duo'>
        <h2>SEO</h2>
        <p>
          For optimized search engine results, we will undertake measures to place you at the top. You need not incur additional costs after the initial launch.
        </p>
      </div>
      <div class = 'child duo order service-icon'>
        <i class = 'icon icon-search'></i>
      </div>
    </div>
    <div class = 'flex tree'>
      <div class = 'child duo service-icon'>
        <i class = 'icon icon-rocket'></i>
      </div>
      <div class = 'child duo'>
        <h2>Blazing Fast</h2>
        <p>
          Fast websites are great for users as they provide a smooth browsing experience. In turn, this
          leads to better conversion rates for your business.
        </p>
      </div>
    </div>
  </section>
</section>
