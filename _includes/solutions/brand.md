<section class = 'weather'>
  <div class = 'about'>
    <div class = 'half'>
        <h1>Brand Design</h1>
        <p> While our experience has taught us that developing a brand is often daunting process, nothing could have 
        prepared us better than those very moments.Our in-house graphics designers will help conceptualize and breathe your 
        new brand to life at a fraction of your budget.</p>
    </div>
  </div>
</section>