<section class = ''>
<div class = 'about weather'>
    <h1>Leverage the online marketplace</h1>
    <div class = 'half'>
        <p>
        Thanks to social media, the internet is now global marketplace.
        We will help you expand awareness for your product online so that you can 
        maximize your reach at a fraction of your budget.
        </p>
    </div>
    <div class = 'flex services'>
        <div class = 'child trio'>
            <h2 class = 'left-text'>Management.</h2>
            <p>We have the depth to create and optimally manage your social media
            accounts. We'll manage your Facebook, Twitter and Pinterest business pages.
            </p>
        </div>
        <div class = 'child trio'>
            <h2 class = 'left-text'>Advertising</h2>
        <p> Are you stuck creating awareness for your products online? We will handle your
        online campaigns, you can sit back and handle other aspects of your business.
        </p>
        </div>
        <div class = 'child trio'>
            <h2 class = 'left-text'>Campaigns.</h2>
            <p> Mobilize people for your cause or event for your organization. We enjoy a challenge every now and then. With Warmhut, you're covered. 
            Ready for your aha moment?</p>
        </div>
    </div>
</div>
</section>