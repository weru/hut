<section class = 'media'>
  <style>
    {% include css/video.css %}
  </style>
  <div class = 'video-box'>
    <video id = 'video'>
      <source src="{{site.baseurl}}/assets/warmhut.mp4" type="video/mp4">
    </video>
    <div id = 'controls'>
      <div id = 'play' class = 'control play tall'><i class = 'icon icon-play'></i></div>
      <div id = 'mute' class = 'control mute'><i class = 'icon icon-mute'></i></div>
      <div id = 'full'  class = 'control full'><i class = 'icon icon-full'></i></div>
      <div id = 'close'  class = 'control close'>Exit</div>
      <input type = 'range' id = 'progress' class = 'control progress range' value = '0'>
    </div>
  </div>
  <script>
  </script>
</section>
